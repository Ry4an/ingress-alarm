import unittest
import util
from textwrap import dedent

class AttackEmailTestCase(unittest.TestCase):
    def test_2_links_html_body(self):
        body = dedent("""\
        Ry4an,<br/><br/>Your Link has been destroyed by Digitalis at
         17:27 hrs. - <a
         href="http://www.ingress.com/intel?latE6=40005196&lngE6=-75259670&z=19">View
         start location</a> - <a
         href="http://www.ingress.com/intel?latE6=40004894&lngE6=-75264360&z=19">View
         end location</a><br/><br/><a
         href="http://www.ingress.com/intel?latE6=40005196&lngE6=-75259670&z=19"><img
         src="http://lh4.ggpht.com/swK203MrrYNmmwy05qHvOjLy7htEAtWU0h_XyFzZSXT8yL5ANTdb2ecU679mxz9uN9Bdj1G42FY1ugDA8bemn
        CmJoRD4krEue9ecd9xDygjxbg9t"/></a>&nbsp;<br/>Your Link has been
         destroyed by Digitalis at 17:27 hrs. - <a
         href="http://www.ingress.com/intel?latE6=40004894&lngE6=-75264360&z=19">View
         start location</a> - <a
         href="http://www.ingress.com/intel?latE6=40005541&lngE6=-75265430&z=19">View
         end
         location</a><br/><br/><br/><br/>------------------------------------------<br/><a
         href="http://www.ingress.com/intel">Dashboard</a>&nbsp;<a
         href="http://support.google.com/ingress">Contact</a><br/>""").replace("\n","")

        attack = util.parse_html_body(body)
        self.assertEqual(attack, {'destroyed': {'resonators': [], 'links': [{'start': {'lat': '40005196', 'long': '-75259670'}, 'time': '17:27', 'end': {'lat': '40004894', 'long': '-75264360'}, 'attacker_codename': 'Digitalis'}, {'start': {'lat': '40004894', 'long': '-75264360'}, 'time': '17:27', 'end': {'lat': '40005541', 'long': '-75265430'}, 'attacker_codename': 'Digitalis'}]}, 'recipient_codename': 'Ry4an', 'image': {'lat': '40005196', 'url': 'http://lh4.ggpht.com/swK203MrrYNmmwy05qHvOjLy7htEAtWU0h_XyFzZSXT8yL5ANTdb2ecU679mxz9uN9Bdj1G42FY1ugDA8bemnCmJoRD4krEue9ecd9xDygjxbg9t', 'long': '-75259670'}})

    def test_2_resonators_1_link_html_body(self):
        body = dedent("""\
        Ry4an,<br/><br/>2 Resonator(s) were destroyed by JS1999 at 09:28 hrs.
         - <a
         href="http://www.ingress.com/intel?latE6=40006679&lngE6=-75260657&z=19">View
         location</a><br/> <br/><br/><br/><br/>Your Link has been destroyed by
         JS1999 at 09:28 hrs. - <a
         href="http://www.ingress.com/intel?latE6=40006679&lngE6=-75260657&z=19">View
         start location</a> - <a
         href="http://www.ingress.com/intel?latE6=40009709&lngE6=-75263851&z=19">View
         end location</a><br/><br/><a
         href="http://www.ingress.com/intel?latE6=40009709&lngE6=-75263851&z=19"><img
         src="http://lh4.ggpht.com/m-5DVyhqdD2MkYVDxm1m75RYsnqHilTPl1BZvuRsJM8Rsrf1uZAkU472RgfRTMI1LGgAWOrhb8OC91g4e9JjS
        7qaIGVnupthLJzXmidgLtG0o9M"/></a><br/><br/><br/>------------------------------------------<br/><a
         href="http://www.ingress.com/intel">Dashboard</a>&nbsp;<a
         href="http://support.google.com/ingress">Contact</a><br/>""").replace("\n","")

        attack = util.parse_html_body(body)
        self.assertEqual(attack, {'destroyed': {'resonators': [{'lat': '40006679', 'attacker_codename': 'JS1999', 'time': '09:28', 'long': '-75260657', 'quantity': 2}], 'links': [{'start': {'lat': '40006679', 'long': '-75260657'}, 'time': '09:28', 'end': {'lat': '40009709', 'long': '-75263851'}, 'attacker_codename': 'JS1999'}]}, 'recipient_codename': 'Ry4an'})
