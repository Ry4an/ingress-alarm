#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import logging
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext import db

####################### models ################################
class Portal(db.Model):
    """ An Ingress Portal """
    location = db.GeoPtProperty()
    name = db.StringProperty()

class Player(db.Model):
    """ A friendly player / user """
    account = db.UserProperty(auto_current_user_add = True)
    code_name = db.StringProperty()
    reporting_email = db.StringProperty()

class AttackReport(db.Model):
    """ Record of something having been destroyed.  Parent should be a portal """
    target = db.ReferenceProperty(Portal)
    when = db.DateTimeProperty()
    attacker = db.StringProperty()
    reporter = db.ReferenceProperty(Player)

####################### handlers ################################
class NotificationMailHandler(InboundMailHandler):
    def receive(self, mail_message):
        logging.info("Received a message from: " + mail_message.sender)
        # extract: reporter.code_name, date, time, target.location, attacker
        # find player
        # find portal
        # create AttackReport
        # fire notifications

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    NotificationMailHandler.mapping(),
], debug=True)
