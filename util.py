import re
import logging

REGEX_SALUTATION = re.compile("^(\w+),$")
REGEX_IMAGE = re.compile('^<a href="http://www.ingress.com/intel\?latE6=(-?\\d+)&lngE6=(-?\\d+)&z=19"><img src="([^"]+)"/></a>&nbsp;$')
REGEX_LINK = re.compile('^Your Link has been destroyed by (\\w+) at (\\d\\d:\\d\\d) hrs. - <a href="http://www.ingress.com/intel\?latE6=(-?\\d+)&lngE6=(-?\\d+)&z=19">View start location</a> - <a href="http://www.ingress.com/intel\?latE6=(-?\\d+)&lngE6=(-?\\d+)&z=19">View end location</a>$')
REGEX_RESONATOR = re.compile('^(\\d+) Resonator\(s\) were destroyed by (\\w+) at (\\d\\d:\\d\\d) hrs. - <a href="http://www.ingress.com/intel\?latE6=(-?\\d+)&lngE6=(-?\\d+)&z=19">View location</a>$')
def parse_html_body(html):
    retval = {'destroyed': {'links': [], 'resonators': []}}
    lines = html.split("<br/>")
    for line in lines:
        logging.debug("LINE '%s'", line)
        m = REGEX_SALUTATION.match(line)
        if m:
            retval['recipient_codename'] = m.group(1)
            continue
        m = REGEX_IMAGE.match(line)
        if m:
            retval['image'] = {'lat': m.group(1), 'long': m.group(2), 'url': m.group(3)}
            continue
        m = REGEX_RESONATOR.match(line)
        if m:
            retval['destroyed']['resonators'].append({
                'quantity': int(m.group(1)),
                'attacker_codename': m.group(2),
                'time': m.group(3),
                'lat': m.group(4),
                'long': m.group(5)
            })
            continue
        m = REGEX_LINK.match(line)
        if m:
            retval['destroyed']['links'].append({
                'attacker_codename': m.group(1),
                'time': m.group(2),
                'start': {
                    'lat': m.group(3),
                    'long': m.group(4)
                },
                'end': {
                    'lat': m.group(5),
                    'long': m.group(6)
                },
            })
            continue
    if not 'recipient_codename' in retval:
        raise Exception("no recipient found")
    if not any(retval['destroyed'].values()):
        raise Exception("nothing destroyed")
    print repr(retval)
    return retval
